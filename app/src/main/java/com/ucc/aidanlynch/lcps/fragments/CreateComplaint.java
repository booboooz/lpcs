package com.ucc.aidanlynch.lcps.fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Toast;

import com.ucc.aidanlynch.lcps.R;
import com.ucc.aidanlynch.lcps.Utilities.AlertDialogRadio;
import com.ucc.aidanlynch.lcps.Utilities.AlertDialogRadio.*;
import com.ucc.aidanlynch.lcps.Utilities.ComplaintTypes;
import com.ucc.aidanlynch.lcps.Utilities.EditDialog;
import com.ucc.aidanlynch.lcps.Utilities.GeoLocation;
import com.ucc.aidanlynch.lcps.activities.TakePhoto;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;





public class CreateComplaint extends ListFragment implements AdapterView.OnItemClickListener, AlertPositiveListener{

   // private ListView list;
    private String GPS;
    private String date;
    public String complaintType;
    private String complaintName;
    private String complaintEmail;
    private String complaintMobile;
    private GeoLocation gps;
    private ArrayAdapter adapter;








    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.create_complaint, container, false);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(), R.array.createComplaintList, android.R.layout.simple_list_item_checked);

        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);






    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CheckedTextView check = (CheckedTextView) view;

        switch (position) {

            case 0:
                gps = new GeoLocation(getActivity());

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();


                    Toast.makeText(getActivity(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();

                    check.setChecked(!check.isChecked());

             GPS = "Latitude: " + latitude + " Longitude: " + longitude;


                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }



                break;
            case 1:
                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                Toast.makeText(getActivity(), currentDateTimeString, Toast.LENGTH_LONG).show();
                date = currentDateTimeString;
             //   list.setItemChecked(position,true);


                check.setChecked(!check.isChecked());

                break;


            case 2:
                FragmentManager manager = getFragmentManager();

                /** Instantiating the DialogFragment class */
                AlertDialogRadio alert = new AlertDialogRadio();

                /** Creating a bundle object to store the selected item's index */
                Bundle b  = new Bundle();

                /** Storing the selected item's index in the bundle object */
                b.putInt("position", position);

                /** Setting the bundle object to the dialog fragment object */
                alert.setArguments(b);

                /** Creating the dialog fragment object, which will in turn open the alert dialog window */
                alert.show(manager, "alert_dialog_radio");


                check.setChecked(!check.isChecked());
                break;

            case 3:

                Intent intent = new Intent(getActivity(), TakePhoto.class);
                startActivity(intent);


                check.setChecked(!check.isChecked());
        }




    }




    @Override
    public void onPositiveClick(int position) {








        /** Setting the selected android version in the textview */


    }

    }









